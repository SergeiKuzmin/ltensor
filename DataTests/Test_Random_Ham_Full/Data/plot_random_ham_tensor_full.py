import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Чтение данных из CSV файла
df = pd.read_csv('./Data/data_ham_r2_ham_r_learn2.csv')

# Фильтрация данных с использованием метода query()
filtered_df = df.query('func_loss_train <= 0.001')

# Агрегация данных по колонкам 'N' и 'm_train' с расчетом среднего значения для 'R2_test'
aggregated_df = filtered_df.groupby(['N', 'm_train'])['R2_test'].mean().reset_index()

# Переименование колонок агрегированной таблицы для удобства
aggregated_df.columns = ['N', 'm_train', 'Average_R2_test']

# Вывод результата
print(aggregated_df)

# Построить графики
plt.figure(figsize=(10, 7))  # Устанавливаем размер фигуры

# Группировка данных по 'N' и построение графиков для каждой группы
unique_N = df['N'].unique()
colors = plt.cm.viridis(np.linspace(0, 1, len(unique_N)))  # Создаем колор мап для уникальных значений N

for i, N in enumerate(unique_N):
    subset = aggregated_df[aggregated_df['N'] == N]
    print(subset)
    plt.scatter(subset['m_train'], subset['Average_R2_test'], lw=5, alpha=0.5, label=f'N = {N}', color=colors[i])
    plt.plot(subset['m_train'], subset['Average_R2_test'], lw=7, alpha=0.5, label=f'N = {N}', color=colors[i])
    # plt.plot(subset['m_train'], subset['Average_R2_test'], label=f'N = {N}', color=colors[i])

plt.tick_params(which='major', direction='in', labelsize=22)
plt.tick_params(which='minor', direction='in', labelsize=22)
# plt.legend(loc='lower right', fontsize=22)

plt.xlabel('M_train', fontsize=22)
plt.ylabel('Average R2 test', fontsize=22)
plt.xlim(3, 1500)
plt.ylim(0.0, 1.1)
# plt.legend()
plt.xscale('log')
plt.grid(True)  # Добавляет сетку для улучшения читаемости
plt.show()

aggregated_filtered_df = aggregated_df.query('Average_R2_test >= 0.9')

aggregated_min_df = aggregated_filtered_df.groupby(['N'])['m_train'].min().reset_index()

print(aggregated_min_df)

# Построить графики
plt.figure(figsize=(10, 6))  # Устанавливаем размер фигуры

# Группировка данных по 'N' и построение графиков для каждой группы
unique_N = df['N'].unique()

plt.scatter(aggregated_min_df['N'], aggregated_min_df['m_train'], lw=5, alpha=0.5, color='green')
plt.plot(aggregated_min_df['N'], aggregated_min_df['m_train'], lw=7, alpha=0.5, color='green')
# plt.plot(subset['m_train'], subset['Average_R2_test'], label=f'N = {N}', color=colors[i])

plt.tick_params(which='major', direction='in', labelsize=22)
plt.tick_params(which='minor', direction='in', labelsize=22)
# plt.legend(loc='lower right', fontsize=22)

plt.xlabel('N', fontsize=22)
plt.ylabel('M_train', fontsize=22)
# plt.title('Average R2_test vs m_train for each N')
#plt.xlim(1, 1500)
#plt.ylim(0.0, 1.1)
# plt.legend()
plt.yscale('log')
plt.grid(True)  # Добавляет сетку для улучшения читаемости
plt.show()
