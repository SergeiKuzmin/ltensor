import json
import numpy as np
import pandas as pd

list_N = [i for i in range(1, 16, 1)]
list_df = []

for N in list_N:
    # Задайте путь к вашему JSON файлу
    file_path = f"./Data/N{N}_ham_r2_ham_r_learn2.json"

    with open(file_path, 'r') as file:
        data = json.load(file)

    # Инициализируем пустой список для хранения данных func_loss_train
    data_all = {'N': N,
                'data': []}

    # Извлекаем данные func_loss_train
    main_data = data.get("main_data", [])
    for k, item in enumerate(main_data):
        data_k = {'k': k,
                  'data_m_train': []}
        data_m_train = item.get("data_m_train", [])
        for entry in data_m_train:
            m_train = entry.get('m_train', [])
            func_loss_train = entry.get('func_loss_train', [])[-1]
            func_loss_test = entry.get('func_loss_test', [])[-1]
            data_E_predict = np.array(entry.get('E_predict', []))
            data_E_exact = np.array(entry.get('E_exact', []))

            std_test = data_E_exact.std()
            error_test = np.mean((data_E_predict - data_E_exact) ** 2)
            R2_test = 1 - error_test / (std_test ** 2)

            m_train_data = {'m_train': m_train,
                            'func_loss_train': func_loss_train,
                            'func_loss_test': func_loss_test,
                            'R2_test': R2_test}
            data_k['data_m_train'].append(m_train_data)
        data_all['data'].append(data_k)

    # print(data_all)

    # Создаем пустой DataFrame
    df = pd.DataFrame(columns=['N', 'm_train', 'k', 'func_loss_train', 'func_loss_test', 'R2_test'])

    # Заполнение DataFrame
    for item in data_all['data']:
        k = item['k']
        for sub_item in item['data_m_train']:
            # Создаем новый ряд данных
            new_row = {
                'N': data['N'],
                'k': k,
                'm_train': sub_item['m_train'],
                'func_loss_train': sub_item['func_loss_train'],
                'func_loss_test': sub_item['func_loss_test'],
                'R2_test': sub_item['R2_test']
            }
            # Добавляем ряд в DataFrame
            df = df.append(new_row, ignore_index=True)

    # print(df)

    list_df.append(df)

# Объединяем все DataFrame в один
combined_df = pd.concat(list_df, ignore_index=True)

print(combined_df)

# Если нужно указать конкретную кодировку, например, UTF-8
combined_df.to_csv('./Data/data_ham_r2_ham_r_learn2.csv', index=False, encoding='utf-8')
