import torch
import os
import sys
import json

from ltensor import DataModelMPSTensor
from ltensor import Info, HamRandom, LearnModelTensorFull

# Считываем аргументы
N = int(sys.argv[1])
ham_r = int(sys.argv[2])
ham_r_learn = int(sys.argv[3])

nums_of_avg = 10

m_train_list = [10 * x + 1 for x in range(100)]
D_train = 1
m_test = 100
D_test = 1

num_of_iters = 5000
max_iter = 10

print("Test CUDA: ", torch.cuda.is_available())
for i in range(torch.cuda.device_count()):
    print("CUDA name: ", torch.cuda.get_device_name(i))

device = "cuda:1"

data = {'N': N,
        'ham_r': ham_r,
        'ham_r_learn': ham_r_learn,
        'nums_of_avg': nums_of_avg,
        'D_train': D_train,
        'D_test': D_test,
        'm_test': m_test,
        'max_num_of_iters': num_of_iters,
        'max_iter': max_iter,
        'device': device,
        'main_data': []}

for k in range(nums_of_avg):
    info = Info(device=device)
    data_model_mps = DataModelMPSTensor(info)

    ham = HamRandom(info)
    ham.generate_ham(N, ham_r)

    data_model_mps.set_ham(ham)
    data_model_mps.gen_data_tensor(max(m_train_list), D_train, m_test, D_test)

    batch = data_model_mps.data_train_tensor

    data_k = {'k': k,
              'data_m_train': []}

    for m_train in m_train_list:
        learn_model = LearnModelTensorFull(info)
        learn_model.gen_start_ham(N, ham_r_learn)

        x = [core[:, :, :, 0:m_train] for core in batch[0]]
        y = batch[1][0:m_train]

        batch_truncated = (x, y)
        data_model_mps.data_train_tensor = batch_truncated

        func_loss_train, func_loss_test = learn_model.optimize_bfgs(data_model_mps, num_of_iters, verbose=True,
                                                                    max_iter=max_iter)

        data_m_train = {'m_train': m_train,
                        'func_loss_train': func_loss_train.tolist(),
                        'func_loss_test': func_loss_test.tolist(),
                        'E_predict': learn_model.mean_energy(data_model_mps.data_test_tensor[0]).cpu().tolist(),
                        'E_exact': data_model_mps.data_test_tensor[1].cpu().tolist()}

        data_k['data_m_train'].append(data_m_train)
    data['main_data'].append(data_k)

# Создаем каталог, если он не существует
directory = "./Data"
if not os.path.exists(directory):
    os.makedirs(directory)

# Используем f-строку для форматирования имени файла
file_name = f"./{directory}/N{N}_ham_r{ham_r}_ham_r_learn{ham_r_learn}.json"

# Откройте файл для записи
with open(file_name, 'w') as f:
    # Используйте json.dump для записи данных в файл
    json.dump(data, f, indent=4)
