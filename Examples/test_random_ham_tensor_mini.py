import torch

from ltensor import DataModelMPSTensor
from ltensor import Info, HamRandom, LearnModelTensorMini

N = 5
ham_r = 2
ham_r_learn = 2

m_train = 300
D_train = 1
m_test = 10
D_test = 1

num_of_iters = 5000
mini_batch_size = 50
max_iter = 1

print("Test CUDA: ", torch.cuda.is_available())
for i in range(torch.cuda.device_count()):
    print("CUDA name: ", torch.cuda.get_device_name(i))

device = "cpu"

info = Info(device=device)
data_model_mps = DataModelMPSTensor(info)

ham = HamRandom(info)
ham.generate_ham(N, ham_r)

data_model_mps.set_ham(ham)
data_model_mps.gen_data_tensor(m_train, D_train, m_test, D_test)

learn_model = LearnModelTensorMini(info)
learn_model.gen_start_ham(N, ham_r_learn)

batch = data_model_mps.data_train_tensor
learn_model.optimize_bfgs(data_model_mps, mini_batch_size, num_of_iters)
