import torch
import numpy as np
from ltensor import HamRandom
from ltensor.qlearn._lbfgs import LBFGS


class LearnModelTensorFull(object):
    def __init__(self, info):
        self.info = info
        self.N = None
        self.x = []

    def gen_start_ham(self, N, r, initial_ham=None):
        """
            Generates random Hamiltonian in MPO format with max max rank equal to r
        """
        self.N = N
        if initial_ham is not None:
            self.x = initial_ham.tt_cores
        else:
            ham_random = HamRandom(self.info)
            ham_random.generate_ham(self.N, r)
            self.x = ham_random.tt_cores

    def mean_energy(self, batch):
        batch_dim = batch[0].shape[3]
        y = [(core + torch.conj(torch.transpose(core, 1, 2))) / 2.0 for core in self.x]
        y_expand = [core.unsqueeze(-1).expand(list(core.shape) + [batch_dim]) for core in y]
        psi_expand = batch

        y_psi = []

        for i in range(0, self.N, 1):
            core_curr = torch.einsum('ijklm,nkpm->injlpm', y_expand[i], psi_expand[i])
            dims = core_curr.shape
            core_curr = torch.reshape(core_curr, (dims[0] * dims[1], dims[2], dims[3] * dims[4], batch_dim))
            y_psi.append(core_curr)

        core_prev = torch.einsum('ijkl,mjol->ikmol', y_psi[0], torch.conj(psi_expand[0]))
        for i in range(1, self.N, 1):
            core_prev = torch.einsum('ijklm,jopm->iklopm', core_prev, y_psi[i])
            core_prev = torch.einsum('ijklmn,kprn->ijlmprn', core_prev, torch.conj(psi_expand[i]))
            core_prev = torch.einsum('ijklknm->iljnm', core_prev)

        mean_energy_predict = core_prev[0, 0, 0, 0, :]

        return np.real(mean_energy_predict)

    def func_loss(self, batch):
        batch_dim = batch[0][0].shape[3]
        y = [(core + torch.conj(torch.transpose(core, 1, 2))) / 2.0 for core in self.x]
        y_expand = [core.unsqueeze(-1).expand(list(core.shape) + [batch_dim]) for core in y]
        psi_expand = batch[0]

        y_psi = []

        for i in range(0, self.N, 1):
            core_curr = torch.einsum('ijklm,nkpm->injlpm', y_expand[i], psi_expand[i])
            dims = core_curr.shape
            core_curr = torch.reshape(core_curr, (dims[0] * dims[1], dims[2], dims[3] * dims[4], batch_dim))
            y_psi.append(core_curr)

        core_prev = torch.einsum('ijkl,mjol->ikmol', y_psi[0], torch.conj(psi_expand[0]))
        for i in range(1, self.N, 1):
            core_prev = torch.einsum('ijklm,jopm->iklopm', core_prev, y_psi[i])
            core_prev = torch.einsum('ijklmn,kprn->ijlmprn', core_prev, torch.conj(psi_expand[i]))
            core_prev = torch.einsum('ijklknm->iljnm', core_prev)

        mean_energy_predict = core_prev[0, 0, 0, 0, :]

        loss = torch.mean(torch.square(mean_energy_predict - batch[1]))
        return loss.real

    def optimize_bfgs(self, data_model, num_of_iters, verbose=True, ftol=1e-10, max_iter=1):
        [x.requires_grad_(True) for x in self.x]

        optimizer = LBFGS(self.x, max_iter=max_iter, line_search_fn='strong_wolfe')

        batch = data_model.data_train_tensor

        func_loss_train = [self.func_loss(batch).item()]
        func_loss_test = [self.func_loss(data_model.data_test_tensor).item()]

        func_loss_curr = self.func_loss(batch).item()

        for iterations in range(num_of_iters):

            def closure():
                optimizer.zero_grad()
                f = self.func_loss(batch)
                f.backward()
                return f

            optimizer.step(closure)

            func_loss_train.append(self.func_loss(batch).item())
            func_loss_test.append(self.func_loss(data_model.data_test_tensor).item())

            if verbose:
                print('Iteration: ', iterations, ' Func loss of train mini_batch: ', func_loss_train[-1],
                      ', Func loss of test data: ', func_loss_test[-1])

            if abs(func_loss_train[-1] - func_loss_curr) <= ftol:
                break

            func_loss_curr = func_loss_train[-1]

        [x.requires_grad_(False) for x in self.x]
        return np.array(func_loss_train), np.array(func_loss_test)
