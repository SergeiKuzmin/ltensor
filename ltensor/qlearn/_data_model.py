import numpy as np
import torch
from ltensor.qtensor import Info, Gates, MPS, MPO


class DataModelBase(object):
    def __init__(self, info):
        self.info = info
        self.N = None
        self.ham = None
        self.data_train = []
        self.data_test = []
        self.data_train_tensor = []
        self.data_test_tensor = []

    def set_ham(self, ham):
        self.N = ham.N
        self.ham = ham

    def gen_data_uni(self, m, D):
        pass

    def gen_data_uni_tensor(self, m, D):
        pass

    def gen_data(self, m_train, D_train, m_test, D_test):
        self.data_train = self.gen_data_uni(m_train, D_train)
        self.data_test = self.gen_data_uni(m_test, D_test)

    def gen_data_tensor(self, m_train, D_train, m_test, D_test):
        self.data_train_tensor = self.gen_data_uni_tensor(m_train, D_train)
        self.data_test_tensor = self.gen_data_uni_tensor(m_test, D_test)

    def get_mini_batch(self, mini_batch_size):
        x = np.random.randint(0, len(self.data_train), mini_batch_size)
        mini_batch_train = [self.data_train[x[i]] for i in range(mini_batch_size)]
        return mini_batch_train


class DataModelMPS(DataModelBase):
    def __init__(self, info):
        super().__init__(info)

    def gen_data_uni(self, m, D):
        data = []
        gates = Gates(self.info)

        for _ in range(m):
            mps = MPS(self.info)
            mps.all_zeros_state(self.N)

            if D == 0:
                list_of_parameters = 2 * np.pi * np.random.rand(3 * self.N)
                for i in range(0, self.N, 1):
                    Rn = gates.Rn(list_of_parameters[3 * i], list_of_parameters[3 * i + 1],
                                  list_of_parameters[3 * i + 2])
                    mps.one_qubit_gate(Rn, i)
            else:
                list_of_parameters = 2 * np.pi * np.random.rand(3 * self.N * D)
                for d in range(D):
                    for i in range(self.N):
                        Rn = gates.Rn(list_of_parameters[3 * self.N * d + 3 * i],
                                      list_of_parameters[3 * self.N * d + 3 * i + 1],
                                      list_of_parameters[3 * self.N * d + 3 * i + 2])
                        mps.one_qubit_gate(Rn, i)
                    for i in range(0, self.N - 1, 1):
                        mps.two_qubit_gate(gates.CX(), i)
            data.append((mps, self.ham.mean_energy(mps)))
        return data


class DataModelMPO(DataModelBase):
    def __init__(self, info):
        super().__init__(info)

    def gen_data_uni(self, m, D):
        data = []

        for _ in range(m):
            mpo = MPO(self.info)
            mpo.gen_random_mpo(self.N, 2 ** D)
            data.append((mpo, self.ham.mean_energy(mpo)))
        return data


class DataModelMPSTensor(DataModelMPS):
    def __init__(self, info):
        super().__init__(info)

    def gen_data_uni_tensor(self, m, D):
        data = self.gen_data_uni(m, D)
        x = [[] for _ in range(self.N)]
        E_list = []
        for mps, E in data:
            E_list.append(E)
            for i in range(len(mps.tt_cores)):
                x[i].append(mps.tt_cores[i])
        y = torch.tensor(E_list, dtype=torch.float, device=self.info.device)
        for idx in range(len(x)):
            x[idx] = torch.stack(x[idx], dim=3)
        return x, y
