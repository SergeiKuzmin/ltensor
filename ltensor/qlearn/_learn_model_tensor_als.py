import torch
import numpy as np
from torch import optim
from ltensor import Info, Gates, MPS, MPO, HamRandom, HamBase
from ltensor.qlearn._lbfgs import LBFGS


class LearnModelTensor(object):
    def __init__(self, info):
        self.info = info
        self.N = None
        self.x = []

    def gen_start_ham(self, N, r, initial_ham=None):
        """
            Generates random Hamiltonian in MPO format with max max rank equal to r
        """
        self.N = N
        if initial_ham is not None:
            self.x = initial_ham.tt_cores
        else:
            ham_random = HamRandom(self.info)
            ham_random.generate_ham(self.N, r)
            self.x = ham_random.tt_cores

    def func_loss(self, batch):
        batch_dim = batch[0][0].shape[3]
        y = [(core + torch.conj(torch.transpose(core, 1, 2))) / 2.0 for core in self.x]
        y_expand = [core.unsqueeze(-1).expand(list(core.shape) + [batch_dim]) for core in y]
        psi_expand = batch[0]

        y_psi = []

        for i in range(0, self.N, 1):
            core_curr = torch.einsum('ijklmnl->ijkmnl', torch.tensordot(y_expand[i], psi_expand[i], dims=([2], [1])))
            core_curr = torch.transpose(core_curr, 2, 3)
            core_curr = torch.transpose(core_curr, 1, 2)
            dims = core_curr.shape
            core_curr = torch.reshape(core_curr, (dims[0] * dims[1], dims[2], dims[3] * dims[4], batch_dim))
            y_psi.append(core_curr)

        core_prev = torch.einsum('ijklmk->ijlmk', torch.tensordot(y_psi[0], torch.conj(psi_expand[0]), dims=([1], [1])))
        for i in range(1, self.N, 1):
            core_prev = torch.einsum('ijklmnl->ijkmnl', torch.tensordot(core_prev, y_psi[i], dims=([1], [0])))
            core_prev = torch.einsum('ijklmnpm->ijklnpm', torch.tensordot(core_prev, torch.conj(psi_expand[i]),
                                                                          dims=([2], [0])))
            core_prev = torch.einsum('ijklknm->ijlnm', core_prev)
            core_prev = torch.transpose(core_prev, 1, 2)

        mean_energy_predict = core_prev[0, 0, 0, 0, :]

        loss = torch.mean(torch.square(mean_energy_predict - batch[1]))
        return loss.real

    def optimize_adam(self, data_model, mini_batch_size, num_of_iters, verbose=True):
        [x.requires_grad_(True) for x in self.omega.tt_cores]

        optimizer = optim.Adam(self.omega.tt_cores)

        mini_batch = data_model.get_mini_batch(mini_batch_size)

        func_loss_train = [self.func_loss(mini_batch).item()]
        func_loss_test = [self.func_loss(data_model.data_test).item()]

        for iterations in range(num_of_iters):
            optimizer.zero_grad()
            mini_batch = data_model.get_mini_batch(mini_batch_size)

            f = self.func_loss(mini_batch)
            f.backward()
            optimizer.step()

            func_loss_train.append(self.func_loss(mini_batch).item())
            func_loss_test.append(self.func_loss(data_model.data_test).item())

            if verbose:
                print('Iteration: ', iterations, ' Func loss of train mini_batch: ', func_loss_train[-1],
                      ', Func loss of test data: ', func_loss_test[-1])

        [x.requires_grad_(False) for x in self.omega.tt_cores]

        return np.array(func_loss_train), np.array(func_loss_test)

    def optimize_bfgs(self, data_model, num_of_iters, verbose=True, max_iter=1):
        [x.requires_grad_(True) for x in self.x]

        optimizer = LBFGS(self.x, max_iter=max_iter, line_search_fn='strong_wolfe')

        batch = data_model.data_train_tensor

        func_loss_train = [self.func_loss(batch).item()]
        func_loss_test = [self.func_loss(data_model.data_test_tensor).item()]

        for iterations in range(num_of_iters):

            def closure():
                optimizer.zero_grad()
                f = self.func_loss(batch)
                f.backward()
                return f

            optimizer.step(closure)

            func_loss_train.append(self.func_loss(batch).item())
            func_loss_test.append(self.func_loss(data_model.data_test_tensor).item())

            if verbose:
                print('Iteration: ', iterations, ' Func loss of train mini_batch: ', func_loss_train[-1],
                      ', Func loss of test data: ', func_loss_test[-1])

        [x.requires_grad_(False) for x in self.x]
        return np.array(func_loss_train), np.array(func_loss_test)
