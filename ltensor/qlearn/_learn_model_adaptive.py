import torch
import numpy as np
from torch import optim
from ltensor import Info, Gates, MPS, MPO, HamRandom, HamBase
from ltensor.qlearn._lbfgs import LBFGS


class LearnModelAdaptive(object):
    def __init__(self, info):
        self.info = info
        self.N = None
        self.omega = None

    def gen_start_ham(self, N, r, initial_ham=None):
        """
            Generates random Hamiltonian in MPO format with max max rank equal to r
        """
        self.N = N
        if initial_ham is not None:
            self.omega = initial_ham
        else:
            ham_random = HamRandom(self.info)
            ham_random.generate_ham(self.N, r)
            self.omega = ham_random

    def get_mean_energy(self, state):
        ham_learn = HamBase(self.info)
        cores = []
        for i in range(self.N):
            cores.append((self.omega.tt_cores[i] + torch.conj(torch.transpose(self.omega.tt_cores[i], 1, 2))) / 2.0)
        ham_learn.N = self.N
        ham_learn.r = self.omega.r
        ham_learn.phys_ind_i = self.omega.phys_ind_i
        ham_learn.phys_ind_j = self.omega.phys_ind_j
        ham_learn.tt_cores = cores
        return ham_learn.mean_energy(state)

    def func_loss(self, batch):
        ham_learn = HamBase(self.info)
        cores = []
        for i in range(self.N):
            cores.append((self.omega.tt_cores[i] + torch.conj(torch.transpose(self.omega.tt_cores[i], 1, 2))) / 2.0)
        ham_learn.N = self.N
        ham_learn.r = self.omega.r
        ham_learn.phys_ind_i = self.omega.phys_ind_i
        ham_learn.phys_ind_j = self.omega.phys_ind_j
        ham_learn.tt_cores = cores
        energy_exact_list = torch.tensor([batch[i][1] for i in range(len(batch))], dtype=torch.float64,
                                         device=self.info.device)
        energy_model_list = torch.zeros(len(batch), dtype=torch.float64, device=self.info.device)
        for i in range(len(batch)):
            energy_model_list[i] = ham_learn.mean_energy(batch[i][0])
        loss = torch.mean(torch.square(energy_model_list - energy_exact_list))
        return loss

    def optimize_adam(self, data_model, mini_batch_size, num_of_iters, verbose=True):
        [x.requires_grad_(True) for x in self.omega.tt_cores]

        optimizer = optim.Adam(self.omega.tt_cores)

        mini_batch = data_model.get_mini_batch(mini_batch_size)

        func_loss_train = [self.func_loss(mini_batch).item()]
        func_loss_test = [self.func_loss(data_model.data_test).item()]

        for iterations in range(num_of_iters):
            optimizer.zero_grad()
            mini_batch = data_model.get_mini_batch(mini_batch_size)

            f = self.func_loss(mini_batch)
            f.backward()
            optimizer.step()

            func_loss_train.append(self.func_loss(mini_batch).item())
            func_loss_test.append(self.func_loss(data_model.data_test).item())

            if verbose:
                print('Iteration: ', iterations, ' Func loss of train mini_batch: ', func_loss_train[-1],
                      ', Func loss of test data: ', func_loss_test[-1])

        [x.requires_grad_(False) for x in self.omega.tt_cores]

        return np.array(func_loss_train), np.array(func_loss_test)

    def optimize_bfgs(self, data_model, mini_batch_size, num_of_iters_als, num_of_iters_bfgs, batch_size=None,
                      verbose=True, max_iter=1):
        if (batch_size is not None) and (batch_size <= len(data_model.data_train)):
            batch = data_model.data_train[0:batch_size]
        else:
            batch = data_model.data_train

        func_loss_train = [self.func_loss(batch).item()]
        func_loss_test = [self.func_loss(data_model.data_test).item()]

        for step in range(num_of_iters_als):
            batch = data_model.data_train[0:int(len(data_model.data_train) * (float(step + 10.0) / num_of_iters_als))]
            for i in range(self.N):
                self.omega.tt_cores[i].requires_grad_(True)

                optimizer = LBFGS([self.omega.tt_cores[i]], max_iter=max_iter)

                def closure():
                    optimizer.zero_grad()
                    f = self.func_loss(batch)
                    f.backward()
                    return f

                optimizer.step(closure)

                func_loss_train.append(self.func_loss(batch).item())
                func_loss_test.append(self.func_loss(data_model.data_test).item())

                if verbose:
                    print('Iteration: ', self.N * step + i, ' Func loss of train mini_batch: ', func_loss_train[-1],
                          ', Func loss of test data: ', func_loss_test[-1])

                self.omega.tt_cores[i].requires_grad_(False)

        [x.requires_grad_(True) for x in self.omega.tt_cores]

        optimizer = LBFGS(self.omega.tt_cores, max_iter=max_iter, line_search_fn='strong_wolfe')

        mini_batch = data_model.get_mini_batch(mini_batch_size)

        func_loss_train = [self.func_loss(mini_batch).item()]
        func_loss_test = [self.func_loss(data_model.data_test).item()]

        for iterations in range(num_of_iters_bfgs):
            mini_batch = data_model.get_mini_batch(mini_batch_size)

            def closure():
                optimizer.zero_grad()
                f = self.func_loss(mini_batch)
                f.backward()
                return f

            optimizer.step(closure)

            func_loss_train.append(self.func_loss(mini_batch).item())
            func_loss_test.append(self.func_loss(data_model.data_test).item())

            if verbose:
                print('Iteration: ', iterations, ' Func loss of train mini_batch: ', func_loss_train[-1],
                      ', Func loss of test data: ', func_loss_test[-1])

        [x.requires_grad_(False) for x in self.omega.tt_cores]
        return np.array(func_loss_train), np.array(func_loss_test)
