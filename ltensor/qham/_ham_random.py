import numpy as np
import torch
from ltensor.qham._ham_base import HamBase


class HamRandom(HamBase):
    def __init__(self, info):
        super().__init__(info)

    def generate_ham(self, N, r):
        self.N = N
        self.phys_ind_i = [2] * N
        self.phys_ind_j = [2] * N
        self.tt_cores = []
        self.r = [1]
        core_init = torch.tensor(np.random.randn(1, self.phys_ind_i[0], self.phys_ind_j[0], r) +
                                 1j * np.random.randn(1, self.phys_ind_i[0], self.phys_ind_j[0], r),
                                 dtype=self.info.data_type, device=self.info.device)
        self.tt_cores.append((core_init + torch.conj(torch.transpose(core_init, 1, 2))) / 2.0)
        self.r.append(r)
        for i in range(1, N - 1, 1):
            core_init = torch.tensor(np.random.randn(r, self.phys_ind_i[0], self.phys_ind_j[0], r) +
                                     1j * np.random.randn(r, self.phys_ind_i[0], self.phys_ind_j[0], r),
                                     dtype=self.info.data_type, device=self.info.device)
            self.tt_cores.append((core_init + torch.conj(torch.transpose(core_init, 1, 2))) / 2.0)
            self.r.append(r)
        core_init = torch.tensor(np.random.randn(r, self.phys_ind_i[0], self.phys_ind_j[0], 1) +
                                 1j * np.random.randn(r, self.phys_ind_i[0], self.phys_ind_j[0], 1),
                                 dtype=self.info.data_type, device=self.info.device)
        self.tt_cores.append((core_init + torch.conj(torch.transpose(core_init, 1, 2))) / 2.0)
        self.r.append(1)
