import torch
import copy
from ltensor.qtensor import MPS, MPO


class HamBase(MPO):
    def __init__(self, info):
        super().__init__(info)

    def create_ham_from_list(self, list_ham):
        ham = list_ham[0]
        for i in range(1, len(list_ham), 1):
            ham = ham.add(list_ham[i])
        self.tt_cores = ham.tt_cores
        self.N = ham.N
        self.r = ham.r
        self.phys_ind_i = ham.phys_ind_i
        self.phys_ind_j = ham.phys_ind_j

    def ham_psi(self, psi: MPS):
        return self.get_product_vector(psi)

    def mean_energy(self, state):
        if isinstance(state, MPO):
            print("Hello from mean_energy!")
            mean_e = self.get_trace_product_matrix(state)
        elif isinstance(state, MPS):
            mean_e = self.get_product_vector(state).scalar_product(state)
        else:
            raise ValueError("State not MPS and not MPO")
        return mean_e.real

    def get_ham_matrix(self):
        return self.get_full_matrix()
