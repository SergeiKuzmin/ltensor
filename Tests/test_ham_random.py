import torch
from ltensor import Info, HamRandom

N = 5
r = 3

info = Info()
ham = HamRandom(info)

ham.generate_ham(N, r)

print(ham.N,  ham.phys_ind_i, ham.phys_ind_j, ham.r)
print(ham.get_ham_matrix())

print(torch.sum(torch.abs(ham.get_ham_matrix() - torch.conj(torch.transpose(ham.get_ham_matrix(), 0, 1))) ** 2))
