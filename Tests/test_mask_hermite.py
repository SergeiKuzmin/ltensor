import torch


def create_hermitian_tensor_v4(r, p):
    # Создаем полностью нулевой тензор комплексных чисел размера (r, p, p, r)
    x = torch.zeros((r, p, p, r), dtype=torch.cfloat)

    # Транспонируемые индексы для формирования эрмитово-сопряженного отношения
    indices_i, indices_j = torch.triu_indices(p, p, 1)
    print(indices_i, indices_j)
    indices_k, indices_l = indices_j, indices_i  # Транспонированные

    # Заполняем случайные комплексные числа в верхний треугольник (k > j)
    random_complex = torch.randn((r, len(indices_i), r), dtype=torch.float) + \
                     1j * torch.randn((r, len(indices_i), r), dtype=torch.float)

    x[:, indices_i, indices_j, :] = random_complex

    # Заполняем сопряженные значения в нижний треугольник (k < j)
    x[:, indices_k, indices_l, :] = torch.conj(random_complex)

    # Заполняем диагонали реальными числами с нулевой мнимой частью
    real_vals = torch.randn((r, p, r), dtype=torch.float)
    complex_diag_vals = real_vals + 0j  # Преобразуем в комплексные с нулевой мнимой частью
    x[:, torch.arange(p), torch.arange(p), :] = complex_diag_vals

    return x


# Пример использования функции
r, p = 3, 4
x = create_hermitian_tensor_v4(r, p)
print(x)

# Проверка условия эрмитово-сопряженности
i, j, k, l = 1, 3, 3, 2  # Изменим для соответствующей индексации
assert torch.allclose(x[i, j, k, l], torch.conj(x[i, k, j, l]))
print("Условие эрмитово-сопряженности выполнено:", x[i, j, k, l], torch.conj(x[i, k, j, l]))
