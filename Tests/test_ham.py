import numpy as np
import torch
from ltensor import Info, Gates, MPS, MPO, HamRandom

N = 3

info = Info()

state = np.random.randn(2 ** N) + 1j * np.random.randn(2 ** N)
pure_state = torch.tensor(state / np.sqrt(np.sum(state * np.conjugate(state))),
                          dtype=info.data_type, device=info.device)

mps = MPS(info)
mps.tt_decomposition(pure_state.reshape([2] * N))

ham = HamRandom(info)
ham.generate_ham(N, 5)

ham_matrix = ham.get_ham_matrix()

print("Test of H * psi")
print(ham.ham_psi(mps).return_full_vector())
print(torch.tensordot(ham_matrix, pure_state, dims=([1], [0])))
print(ham.r, mps.r, ham.ham_psi(mps).r)

print()
print("Test of psi * H * psi")
print(ham.mean_energy(mps))
print(torch.tensordot(torch.tensordot(ham_matrix, pure_state, dims=([1], [0])), torch.conj(pure_state),
                      dims=([0], [0])))

mpo = MPO(info)
mpo.gen_random_mpo(N, 7)

print()
print("Test of H * rho")
print(ham.get_product_matrix(mpo).get_full_matrix())
print(torch.tensordot(ham.get_ham_matrix(), mpo.get_full_matrix(), dims=([1], [0])))

print()
print("Test of Tr(rho * H)")
print(ham.mean_energy(mpo))
print(ham.get_trace_product_matrix(mpo), mpo.get_trace_product_matrix(ham), ham.get_product_matrix(mpo).get_trace())
print(torch.trace(torch.tensordot(mpo.get_full_matrix(), ham.get_ham_matrix(), dims=([1], [0]))))
