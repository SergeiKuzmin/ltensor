import torch

from ltensor import Info, HamRandom, DataModelMPSTensor
from ltensor import Info, MPS, MPO, HamRandom, DataModelMPS, DataModelMPO, LearnModelTensor

N = 7
r = 2
ham_r = 2
ham_r_learn = 2

m_train = 300
D_train = 1
m_test = 10
D_test = 1

num_of_iters = 5000
mini_batch_size = 5
max_iter = 3

info = Info()
data_model_mps = DataModelMPSTensor(info)

ham = HamRandom(info)
ham.generate_ham(N, ham_r)

data_model_mps.set_ham(ham)
# data_model_mps.gen_data(m_train, D_train, m_test, D_test)
data_model_mps.gen_data_tensor(m_train, D_train, m_test, D_test)

learn_model = LearnModelTensor(info)
learn_model.gen_start_ham(N, ham_r_learn)

batch = data_model_mps.data_train_tensor
learn_model.optimize_bfgs(data_model_mps, num_of_iters)
