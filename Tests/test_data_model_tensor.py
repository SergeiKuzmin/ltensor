from ltensor import Info, HamRandom, DataModelMPSTensor

N = 10
r = 8
ham_r = 5

m_train = 10
D_train = 2
m_test = 5
D_test = 3

info = Info()
data_model_mps = DataModelMPSTensor(info)

ham = HamRandom(info)
ham.generate_ham(N, ham_r)

data_model_mps.set_ham(ham)
data_model_mps.gen_data(m_train, D_train, m_test, D_test)
data_model_mps.gen_data_tensor(m_train, D_train, m_test, D_test)

# print(len(data_model_mps.data_train), len(data_model_mps.data_test))
# print(data_model_mps.data_train[0][0].r, data_model_mps.data_test[0][0].r)
# print(data_model_mps.data_train_tensor, data_model_mps.data_test_tensor)
print(data_model_mps.data_train_tensor[0][9].size())
print(data_model_mps.data_train_tensor[0][0].shape[3])
# print(data_model_mps.data_train_tensor[0].size(), data_model_mps.data_train_tensor[1].size(), data_model_mps.data_test_tensor[0].size(),
#       data_model_mps.data_test_tensor[1].size())
