import torch

x = torch.tensor([1, 2, 3])

print(x.shape)
print(x.unsqueeze(-1).expand(3, 5))
