import torch
from ltensor import Info, Gates, MPS

N = 5

info = Info()
gates = Gates(info)

mps_a = MPS(info)
mps_b = MPS(info)

# mps_a.all_zeros_state(N)
# mps_b.all_zeros_state(N)

a = torch.reshape(torch.randn(2 ** N, dtype=info.data_type, device=info.device), [2] * N)
b = torch.reshape(torch.randn(2 ** N, dtype=info.data_type, device=info.device), [2] * N)

mps_a.tt_decomposition(a)
mps_b.tt_decomposition(b)

print(a.reshape(-1))
print(mps_a.return_full_vector())

mps_c = MPS.add(mps_a, mps_b)
print(mps_a.r, mps_b.r, mps_c.r)
print(mps_c.phys_ind)
c = mps_c.return_full_vector()

print(c)
print((a + b).reshape(-1))

print(torch.sum(torch.abs((c - (a + b).reshape(-1))) ** 2))
