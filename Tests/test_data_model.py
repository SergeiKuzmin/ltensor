from ltensor import Info, HamRandom, DataModelMPS, DataModelMPO

N = 10
r = 8

m_train = 10
D_train = 2
m_test = 5
D_test = 3

info = Info()
data_model_mps = DataModelMPS(info)

ham = HamRandom(info)
ham.generate_ham(N, 5)

data_model_mps.set_ham(ham)
data_model_mps.gen_data(m_train, D_train, m_test, D_test)

print(len(data_model_mps.data_train), len(data_model_mps.data_test))
