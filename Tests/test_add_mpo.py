import torch
from ltensor import Info, Gates, MPO

N = 5

info = Info()
gates = Gates(info)

mpo_a = MPO(info)
mpo_b = MPO(info)

# mpo_a.all_zeros_state(N)
# mpo_b.all_zeros_state(N)

a = torch.reshape(torch.randn(2 ** (2 * N), dtype=info.data_type, device=info.device), [2] * (2 * N))
b = torch.reshape(torch.randn(2 ** (2 * N), dtype=info.data_type, device=info.device), [2] * (2 * N))

mpo_a.tt_decomposition(a)
mpo_b.tt_decomposition(b)

print(a.reshape((2 ** N, 2 ** N)))
print(mpo_a.get_full_matrix())

mpo_c = MPO.add(mpo_a, mpo_b)
print(mpo_a.r, mpo_b.r, mpo_c.r)
print(mpo_c.phys_ind_i, mpo_c.phys_ind_j)
c = mpo_c.get_full_matrix()

print(c)
print((a + b).reshape((2 ** N, 2 ** N)))

print(torch.sum(torch.abs((c - (a + b).reshape((2 ** N, 2 ** N)))) ** 2))
