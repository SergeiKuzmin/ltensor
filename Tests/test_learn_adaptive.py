import torch

from ltensor import Info, MPS, MPO, HamRandom, DataModelMPS, DataModelMPO, LearnModelAdaptive

# Task parameters
N = 5
r_ham = 2
r_ham_learn = 2
M = 150
m = 20
D_state_train = 4
D_state_test = 4
num_of_iters_als = 1
num_of_iters_bfgs = 1500
mini_batch_size = 5
max_iter = 3

info = Info()

ham = HamRandom(info)
ham.generate_ham(N, r_ham)

data_model = DataModelMPS(info)
learn_model = LearnModelAdaptive(info)

data_model.set_ham(ham)
data_model.gen_data(M, D_state_train, m, D_state_test)
# learn_model.gen_start_ham(N, r_ham_learn, initial_ham=ham)
learn_model.gen_start_ham(N, r_ham_learn)

# Check ranks
print(data_model.ham.r)
print(learn_model.omega.r)

# func_loss_train, func_loss_test = learn_model.optimize_adam(data_model, mini_batch_size, num_of_iters, verbose=True)
func_loss_train, func_loss_test = learn_model.optimize_bfgs(data_model, mini_batch_size, num_of_iters_als,
                                                            num_of_iters_bfgs)

for i in range(len(data_model.data_test)):
    print(learn_model.get_mean_energy(data_model.data_test[i][0]), data_model.data_test[i][1],
          (learn_model.get_mean_energy(data_model.data_test[i][0]) - data_model.data_test[i][1]) /
          data_model.data_test[i][1])
